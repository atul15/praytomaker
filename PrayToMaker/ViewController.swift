//
//  ViewController.swift
//  PrayToMaker
//
//  Created by Ankur Gupta on 18/10/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imgPray: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDetail: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {
            print("Lang already set")
        } else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
            print("Lang now set eng")
        }
        let jeremyGif = UIImage.gifImageWithName("pray")
        
        imgPray.image = jeremyGif

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTextOnChange()
        
    }
    func setTextOnChange()
    {
        self.lblHeading.text = ("hello".localized3)

        
        lblDetail.text = ("pray".localized3)
    }
    @IBAction func showSetting(_ sender: Any) {
        print("showSetting")
        let alertController = UIAlertController(title: "Setting's!", message: "Language Setting", preferredStyle: .alert)
        let engAction = UIAlertAction(title: "English", style: .default) { (action) in
            self.setLanguageOnSelection(slct: "en")
        }
        let hinAction = UIAlertAction(title: "Hindi", style: .default) { (action) in
            self.setLanguageOnSelection(slct: "hi")
        }
        let canAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
        }
        alertController.addAction(engAction)
        alertController.addAction(hinAction)
        alertController.addAction(canAction)

        self.present(alertController, animated: true) {
            
        }
    }
    func setLanguageOnSelection(slct:String){
        UserDefaults.standard.set(slct, forKey: "i18n_language")
        UserDefaults.standard.synchronize()
        setTextOnChange()
    }
}

extension String {
    var localized3: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }

        let lang = UserDefaults.standard.string(forKey: "i18n_language")

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
