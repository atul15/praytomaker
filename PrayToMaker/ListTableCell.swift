//
//  ListTableCell.swift
//  PrayToMaker
//
//  Created by Ankur Gupta on 19/10/21.
//

import UIKit

class ListTableCell: UITableViewCell {

    
    @IBOutlet weak var rwImage: UIImageView!
    @IBOutlet weak var rwTitleLabel: UILabel!
    @IBOutlet weak var rwFavStatusBtn: UIButton!
    @IBOutlet weak var rwFavStatusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
