//
//  ListPageController.swift
//  PrayToMaker
//
//  Created by Ankur Gupta on 18/10/21.
//

import UIKit
struct DevObject
{
    var name:String!
    var aarti:String!
    var imgName:String!
    var numberOfImage:Int!
    var soundhindi:String!
    
    init(nm:String,art:String,img:String,num:Int,aufile:String)
    {
        name=nm
        aarti=art
        imgName=img
        numberOfImage=num
        soundhindi=aufile
    }
}
class ListPageController: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnBkDealer: UIButton!
    @IBOutlet weak var btnDate: UIButton!
    var tblArr: [DevObject] = Array()

    @IBOutlet weak var rptTable : UITableView!

    @IBAction func bkFromList(_ sender: Any) {
        print("bkFromList")
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblArr.append(DevObject(nm: "ganesha", art: "aartiganesh", img: "ganesha", num: 4, aufile: "ganpati")) //sound *
        tblArr.append(DevObject(nm: "shiva", art: "aartishiv", img: "Shiva", num: 5, aufile:"shiva"))  //sound
        tblArr.append(DevObject(nm: "durga", art: "aartidurga", img: "durga", num: 6, aufile:"durga"))  //sound
        tblArr.append(DevObject(nm: "kartikey", art: "aartikartikey", img: "kartikey", num: 5, aufile:"Kartik"))

        
        tblArr.append(DevObject(nm: "vishnu", art: "aartivishnu", img: "vishnu", num: 7, aufile:"vishnu")) //sound
        tblArr.append(DevObject(nm: "laxmi", art: "aartilaxmi", img: "goddess_lakshmi", num: 6, aufile:"laxmi")) //sound
        
        tblArr.append(DevObject(nm: "santoshi", art: "aartisantoshi", img: "santoshi", num: 8, aufile:"santoshi"))//sound
        tblArr.append(DevObject(nm: "kali", art: "aartikali", img: "kali", num: 10, aufile:"kali"))//sound
       
        tblArr.append(DevObject(nm: "sarswati", art: "aartisarswati", img: "saraswati", num: 5, aufile:"Saraswati"))//
        tblArr.append(DevObject(nm: "gayatri", art: "aartigayatri", img: "gayatri", num: 5, aufile:"Gayatri"))//

       
        tblArr.append(DevObject(nm: "ram", art: "aartiram", img: "ram", num: 6, aufile:"ram")) //sound
        tblArr.append(DevObject(nm: "krishna", art: "aartikrishna", img: "krishna", num: 12, aufile:"krishna")) //sound
        tblArr.append(DevObject(nm: "hanuman", art: "aartihanuman", img: "hanuman", num: 6, aufile: "hanuman"))//sound
        
        tblArr.append(DevObject(nm: "shani", art: "aartishani", img: "shani-dev", num: 5, aufile:"shani"))//sound
        tblArr.append(DevObject(nm: "surya", art: "aartisurya", img: "surya", num: 4, aufile:"surya"))//sound
        tblArr.append(DevObject(nm: "bhairav", art: "aartibhairav", img: "bhairav", num: 8, aufile:"bharav"))
        
        tblArr.append(DevObject(nm: "sain", art: "aartisain", img: "sain", num: 5, aufile:"sain"))//sound
        tblArr.append(DevObject(nm: "ganga", art: "aartiganga", img: "ganga", num: 13, aufile:"ganga"))
        
       
        tblArr.append(DevObject(nm: "satyanaryan", art: "aartisatnarayan", img: "sataynarayan", num: 6, aufile:"satyanarayan"))//sound
        tblArr.append(DevObject(nm: "swaminaryan", art: "aartiswaminarayan", img: "swaminarayan", num: 12, aufile:"SWAMINARAYAN"))


        
        
        // Do any additional setup after loading the view.
        rptTable.delegate=self
        rptTable.dataSource=self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblHeading.text = ("welcome".localized3)
        self.btnBkDealer.setTitle(("back".localized3), for: .normal)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tblArr.count
    }

   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     print("In cellForRowAt ")
         guard let cell = tableView.dequeueReusableCell(withIdentifier: "RwdCell", for: indexPath) as? ListTableCell else {
             fatalError("Unable to dequeue ReminderCell")
         }
        cell.rwImage.backgroundColor = .white
        cell.rwImage.layer.cornerRadius = 10
        let devObj = self.tblArr[indexPath.row]
        //cell.rwTitleLabel.text = self.tblArr[indexPath.row]
        cell.rwTitleLabel.text = (devObj.name.localized)
        cell.rwImage.image = UIImage.init(named: devObj.imgName+"-icon.jpg")
        cell.rwFavStatusBtn.isHidden = true
        // cell.rwdSalesNumberLabel.attributedText = makeAttributedString(str1: "Sales No: ", str2: rwdDt.sales_no,forField: 2)
    
         return cell
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController:DetailPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailPage") as! DetailPage
        viewController.selectedDevObj=self.tblArr[indexPath.row]
        viewController.fultblArr=tblArr
         self.navigationController?.pushViewController(viewController, animated: true)
    }

}
extension String {
    var localized2: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }

        let lang = UserDefaults.standard.string(forKey: "i18n_language")

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

