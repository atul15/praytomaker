//
//  DetailPage.swift
//  PrayToMaker
//
//  Created by Ankur Gupta on 18/10/21.
//https://github.com/RastislavMirek/FlexColorPicker

import UIKit
import Foundation
import AVFoundation
class DetailPage: UIViewController,AVAudioPlayerDelegate {
    var soundPlayer:AVAudioPlayer!
    var fultblArr: [DevObject] = Array()
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnBk: UIButton!

    @IBOutlet weak var btnFont: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnBgCol: UIButton!
    @IBOutlet weak var btnTxtCol: UIButton!
    @IBOutlet weak var btnLoop: UIButton!

    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var lowerView: UIView!

    
    @IBOutlet weak var slider: UISlider!

    var MainScrollView: UIScrollView!
    @IBOutlet weak var mainTextContainer: UITextView!

 
    var scrollImageTimer: Timer!
    var timer: Timer!
    
    var images = [String]()
    let indexSelected:Int = 0
    var isLoadedd:Bool!
    var isOpenForText:Bool!

    var selectedDevObj:DevObject!
    var currentSoundLevel:Float = 0.0
  
    var currentAlphaLevel:CGFloat = 0.0
    var currentFontSize:CGFloat = 0.0
    
    @IBOutlet weak var sliderFont: UISlider! {
        didSet {
            sliderFont.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
        } // didSet
    } // IBOutlet
    

    @IBAction func btnSoundMuteClicked(_ sender: Any) {
        print("btnSoundMuteClicked")
        if btnMute.backgroundImage(for: .normal) == UIImage.init(named: "sound_on")
        {
            btnMute.setBackgroundImage(UIImage.init(named: "sound_off"), for: .normal)
          currentSoundLevel = soundPlayer!.volume

        }
        else
        {
           soundPlayer?.volume = currentSoundLevel
            btnMute.setBackgroundImage(UIImage.init(named: "sound_on"), for: .normal)
        }
    }
    @IBAction func btnLoopClicked(_ sender: Any) {
        print("btnLoopClicked")
        
        if btnLoop.alpha == currentAlphaLevel
        {
            btnLoop.alpha = 1.0
        }
        else if btnLoop.alpha != currentAlphaLevel
        {
            btnLoop.alpha = currentAlphaLevel
        }
        print("btnLoopClicked btnLoop.alpha = \(btnLoop.alpha)")

    }
    @IBAction func btnFontClicked(_ sender: Any) {
        print("btnFontClicked")
        if sliderFont.isHidden
        {
            sliderFont.isHidden = false
        }
        else
        {
            sliderFont.isHidden = true
        }
        
        
    }
    
    @IBAction func btnTxtColorClicked(_ sender: Any) {
        print("btnTxtColorClicked")
        isOpenForText = true
        if #available(iOS 14.0, *) {
            openColorPicker()
        } else {
            // Fallback on earlier versions
        }
    }
    @IBAction func btnTBgColorClicked(_ sender: Any) {
        print("btnTBgColorClicked")
        isOpenForText = false
        if #available(iOS 14.0, *) {
            openColorPicker()
        } else {
            // Fallback on earlier versions
        }
    }
    
 
    @available(iOS 14.0, *) func openColorPicker()
    {
        // Initializing Color Picker
       
        let picker = UIColorPickerViewController()
        

        // Setting the Initial Color of the Picker
        picker.selectedColor = self.view.backgroundColor!

        // Setting Delegate
        picker.delegate = self

        // Presenting the Color Picker
        self.present(picker, animated: true, completion: nil)
    }
    @IBAction func bkFromDetail(_ sender: Any) {
        print("bkFromDetail")
        if isLoadedd && soundPlayer.isPlaying
        {
            soundPlayer.stop()
        }
        if (timer != nil)
        {
            timer.invalidate()
        }
        self.navigationController?.popViewController(animated: true)
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("audioPlayerDidFinishPlaying")
        if btnLoop.alpha != currentAlphaLevel
        {
            soundPlayer.play()
        }
        else
        {
            btnPlay.setBackgroundImage(UIImage(named: "play"), for: .normal)

        }
        
    }
    @IBAction func sliderFontValueChanged(_ sender: Any) {
        print("sliderValueChanged '\(sliderFont.value)")
        
        currentFontSize = CGFloat(sliderFont.value)
        mainTextContainer.font = UIFont(name: mainTextContainer.font!.fontName, size: currentFontSize)
        currentFontSize = mainTextContainer.font!.pointSize
        print("current font=\(currentFontSize)")
        
              
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBgCol.isHidden = true
        btnTxtCol.isHidden = true
        
        
        
        isOpenForText = false
        self.btnBk.setTitle(("back".localized3), for: .normal)
        btnMute.setBackgroundImage(UIImage.init(named: "sound_on"), for: .normal)
        btnLoop.setBackgroundImage(UIImage.init(named: "loop-icon"), for: .normal)
        btnFont.setBackgroundImage(UIImage.init(named: "font-icon"), for: .normal)
        btnFont.setTitle("", for: .normal)
        btnLoop.alpha = 0.4
        currentAlphaLevel = btnLoop.alpha
        sliderFont.isHidden = true
        currentFontSize = mainTextContainer.font!.pointSize
        print("current font=\(currentFontSize)")
      
        sliderFont.minimumValue = 10
        sliderFont.maximumValue = 22
        sliderFont.isContinuous = true
        
        // Do any additional setup after loading the view.
        self.slider.value = 0.0
        MainScrollView = UIScrollView()
       // mainTextContainer = UITextView()
        self.lblHeading.text = (selectedDevObj.name.localized)
        
        images = getImageArray()
       setScrollableBackground()
       setTimer()
        setUpTextNow()
        
        let strFile = selectedDevObj.soundhindi+".mp3"
        print("Sound file name : \(strFile) ")
        let path = Bundle.main.path(forResource: selectedDevObj.soundhindi+".mp3", ofType:nil)!
        let url = URL(fileURLWithPath: path)

        do {
            soundPlayer = try AVAudioPlayer(contentsOf: url)
            print("File loaded ")
            initializeAndPlay()
        } catch {
            isLoadedd = false
            print("File load failed")
            
            let path = Bundle.main.path(forResource:"shiva.mp3", ofType:nil)!
            let url = URL(fileURLWithPath: path)
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: url)
                print("shiva File loaded ")
                initializeAndPlay()
            }

            catch {
               isLoadedd = false
                print("shiva File loaded ")
            }
            
            
        }
       
        if #available(iOS 14, *) {
            print("btn hidden set false")
            btnBgCol.isHidden = false
            btnTxtCol.isHidden = false
            
            self.upperView.bringSubviewToFront(btnBgCol)
            self.upperView.bringSubviewToFront(btnTxtCol)

        }
        
        btnMute.setBackgroundImage(UIImage.init(named: "sound_on"), for: .normal)
    }
    func initializeAndPlay()
    {
        
      // soundPlayer?.volume = 0.0
        soundPlayer.delegate = self
  
        slider.minimumValue = 0
                
           let ti = NSInteger(soundPlayer.duration)
             let seconds = ti % 60
        print("duration=\(ti)")

        print("Seconds=\(seconds)")
        
   
        slider.maximumValue = Float(ti)

        slider.isContinuous = true
        
        isLoadedd = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:  #selector(updateTimer), userInfo: nil, repeats: true)
        soundPlayer?.play()
        btnPlay.setTitle("", for: .normal)
        btnPlay.backgroundColor = .clear
        btnPlay.setBackgroundImage(UIImage(named: "pause"), for: .normal)
    }
    @objc func updateTimer() {
       // print("updateTimer \(self.soundPlayer?.currentTime)")

        if soundPlayer != nil && soundPlayer.isPlaying   {
            slider.setValue(Float((self.soundPlayer?.currentTime)!), animated: true)
           // slider.setValue(Float((self.soundPlayer?.currentTime)! / (self.soundPlayer?.duration)!), animated: false)
        }
    }
    
    func getImageArray() -> [String]
    {
        var ar = [String]()
        
        for n in 1...selectedDevObj.numberOfImage {
            var str = selectedDevObj.imgName!+"-\(n)"
            ar.append(str+".jpg")
          //  ar.append("\(String(describing: selectedDevObj.imgName))"+"-\(n).jpg")
        }

       print("getImageArray :\(ar)")
        return ar
     /*   if indexSelected == 0
        {
            return ["ganesha-1.jpg","ganesha-2.jpg","ganesha-3.jpg","ganesha-4.jpg"]
        }
        else if indexSelected == 1
        {
            return ["shiva-1.jpg","shiva-2.jpg","shiva-3.jpg","shiva-4.jpg"]
        }
        else if indexSelected == 2
        {
            return ["Durga-1.jpg","Durga-2.jpg","Durga-3.jpg","Durga-4.jpg"]
        }
        else if indexSelected == 3
        {
            return ["Vishnu-1.jpg","Vishnu-2.jpg","Vishnu-3.jpg","Vishnu-4.jpg"]
        }
        else if indexSelected == 4
        {
            return ["Laxmi-1.jpg","Laxmi-2.jpg","Laxmi-3.jpg","Laxmi-4.jpg"]
        }
        else if indexSelected == 5
        {
            return ["Santoshi-1.jpg","Santoshi-2.jpg","Santoshi-3.jpg","Santoshi-4.jpg"]
        }
        else if indexSelected == 6
        {
            return ["Rama-1.jpg","Rama-2.jpg","Rama-3.jpg","Rama-4.jpg"]
        }
        else if indexSelected == 7
        {
            return ["Krishna-1.jpg","Krishna-2.jpg","Krishna-3.jpg","Krishna-4.jpg"]
        }
        else if indexSelected == 8
        {
            return ["Hanuman-1.jpg","Hanuman-2.jpg","Hanuman-3.jpg","Hanuman-4.jpg"]
        }
        else if indexSelected == 9
        {
            return ["Surya-1.jpg","Surya-2.jpg","Surya-3.jpg","Surya-4.jpg"]
        }
        else
        {
            return ["Shani-1.jpg","Shani-2.jpg","Shani-3.jpg","Shani-4.jpg"]
        }*/
    }
    func setScrollableBackground()
    {
    self.MainScrollView.frame = self.upperView.frame
        self.upperView.addSubview(self.MainScrollView)
    self.MainScrollView.frame = CGRect(x:0, y:0, width:self.upperView.frame.width, height:self.upperView.frame.height)
    //width of scrollView
    let scrollViewWidth:CGFloat = self.MainScrollView.frame.width
    //height of scrollView
    let scrollViewHeight:CGFloat = self.MainScrollView.frame.height

    for index in 0..<images.count {
    let imageView = UIImageView(frame: CGRect(x: CGFloat(index) * scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
    //Assign image to imageView based on index of images array
    imageView.image = UIImage(named: images[index])
    //Set imageView contentMode as .scaleAspectFit (or) .scaleAspectFill (or) .scaleToFill
    imageView.contentMode = .scaleAspectFill
    // add imageView as a subview of scrollView
    self.MainScrollView.addSubview(imageView)
    }
    //Assign total content size to scrollView to allow scrolling based on the width and no of images in images array.
    self.MainScrollView.contentSize = CGSize(width:self.MainScrollView.frame.width * CGFloat(images.count), height: self.MainScrollView.frame.height)
        self.MainScrollView.isPagingEnabled = true
    }

    func setTimer() {
    scrollImageTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    func setUpTextNow()  {
        /*self.mainTextContainer.frame = self.lowerView.frame
            self.lowerView.addSubview(self.mainTextContainer)
        self.mainTextContainer.frame = CGRect(x:0, y:0, width:self.lowerView.frame.width, height:self.lowerView.frame.height)
        self.mainTextContainer.isEditable = false*/
        self.mainTextContainer.text = getTextToShow()
        
      //  self.mainTextContainer.setContentOffset(.zero, animated: true)
        
        let bottomOffset = CGPoint(x: 0, y: 0)
            self.mainTextContainer.setContentOffset(bottomOffset, animated: true)
        
    }
    @objc func moveToNextPage() {
    //width of scrollView
    let pageWidth:CGFloat = self.MainScrollView.frame.width
    //Maximum width of scrollView based on number of images needed to animate
    let maxWidth:CGFloat = pageWidth * CGFloat(images.count)
    let contentOffset:CGFloat = self.MainScrollView.contentOffset.x
    var slideToX = contentOffset + pageWidth
    if contentOffset + pageWidth == maxWidth {
    //When attain maximum width of scrollView make X = 0 to start from beginning.
    slideToX = 0
    }
    //make scrollView scorll automatically based on the width and no of elements in array
    self.MainScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.MainScrollView.frame.height), animated: true)
    }

    //aartisain,aartishani,aartihanuman,aartikrishna,aartiram,aartisantoshi,aartilaxmi,aartivishnu,aartidurga,aartishiv,aartiganesh
    @IBAction func btnPlayClick(_ sender: Any) {
        print("btnPlayClick")
        if isLoadedd
        {
          if soundPlayer.isPlaying
        {
            soundPlayer.stop()
              btnPlay.setBackgroundImage(UIImage(named: "play"), for: .normal)

        }
            else{
                soundPlayer.play()
                btnPlay.setBackgroundImage(UIImage(named: "pause"), for: .normal)

            }
            }
        
    }
    @IBAction func sliderValueChanged(_ sender: Any) {
        print("sliderValueChanged '\(slider.value)")
              
        soundPlayer.currentTime =  TimeInterval(slider.value)
 
              if soundPlayer!.rate == 0
              {
                  soundPlayer?.play()
                  btnPlay.setBackgroundImage(UIImage(named: "pause"), for: .normal)

              }
    }
    
    func getTextToShow() -> String {
        
        return ((selectedDevObj.aarti.localized))

    }
    


}
extension String {
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }

        let lang = UserDefaults.standard.string(forKey: "i18n_language")

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
extension DetailPage: UIColorPickerViewControllerDelegate {
    
    //  Called once you have finished picking the color.
    @available(iOS 14.0, *)
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        //self.view.backgroundColor = viewController.selectedColor
        if isOpenForText {
           
            mainTextContainer.textColor =  viewController.selectedColor
        }
        else{
            mainTextContainer.backgroundColor = viewController.selectedColor

        }
        
    }
    
    //  Called on every color selection done in the picker.
    @available(iOS 14.0, *)
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        if isOpenForText {
           
            mainTextContainer.textColor =  viewController.selectedColor
        }
        else{
            mainTextContainer.backgroundColor = viewController.selectedColor

        }
    }
}
